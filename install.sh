#!/bin/bash
echo "updating system"
sudo apt-get update
echo "Installing Unclutter"
sudo apt-get install unclutter
echo "Installing autostart script"
sudo cp autostart /home/pi/.config/lxsession/LXDE-pi/autostart
echo "Copy html file"
sudo cp index.html /home/pi/index.html
echo "Copy Chromium Preferences file"
sudo cp Preferences /home/pi/Preferences
echo "Setting up wallpaper"
x=$(pwd)
w="wallpaper.jpg"
p=$x"/"$w
pcmanfm -w --set-wallpaper=$p --wallpaper-mode=fit

